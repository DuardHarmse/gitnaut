﻿using Chromely.Core.RestfulService;
using Gitnaut.Core;
using Gitnaut.Core.Models;
using LibGit2Sharp;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace ChromelyVueCefSharp.Controllers
{
    [ControllerProperty(Name = nameof(GitnautController), Route = "gitnaut")]
    public class GitnautController : ChromelyController
    {
        public GitnautController()
        {
            RegisterPostRequest("/gitnaut/clone", Clone);
            RegisterPostRequest("/gitnaut/checkout", Checkout);
            RegisterPostRequest("/gitnaut/repositories", Branches);
        }

        private ChromelyResponse Clone(ChromelyRequest request)
        {
            var response = new ChromelyResponse();

            try
            {
                CloneRequest cloneRequest = JsonConvert.DeserializeObject<CloneRequest>(JsonConvert.SerializeObject(request.PostData));
                string repository = new GitnautCore().Clone(cloneRequest);
                response.Data = repository;
            }
            catch (Exception ex)
            {
                response.Data = ex.Message;
            }

            return response;
        }

        private ChromelyResponse Checkout(ChromelyRequest request)
        {
            var response = new ChromelyResponse();

            try
            {
                CheckoutRequest checkoutRequest = JsonConvert.DeserializeObject<CheckoutRequest>(JsonConvert.SerializeObject(request.PostData));
                Branch branch = new GitnautCore().Checkout(checkoutRequest);
                response.Data = branch.FriendlyName;
            }
            catch (Exception ex)
            {
                response.Data = ex.Message;
            }

            return response;
        }

        private ChromelyResponse Branches(ChromelyRequest request)
        {
            var response = new ChromelyResponse();

            try
            {
                response.Data = new GitnautCore().Repositories().ToArray();
            }
            catch (Exception ex)
            {
                response.Data = ex.Message;
            }

            return response;
        }
    }
}
