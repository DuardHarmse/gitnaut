import Vue from 'vue'
import Router from 'vue-router'

import Home from './Home.vue'
import Demo from './Demo.vue'
import Gitnaut from './Gitnaut'
import GitnautThin from './GitnautThin'

Vue.use(Router)

const routes = [
  { path: '/', component: Home },
  { path: '/demo', component: Demo },
  { path: '/gitnaut', component: Gitnaut },
  { path: '/gitnaut-thin', component: GitnautThin },
  { path: '*', redirect: '/' }
]

export default new Router({
  routes
})
