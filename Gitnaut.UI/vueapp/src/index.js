import Vue from 'vue'
import Vuetify from 'vuetify'
import BootstrapVue from "bootstrap-vue"
import router from './router'
import App from './App'
import 'vuetify/dist/vuetify.min.css'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import "./assets/css/chromely.css"

Vue.use(Vuetify, {
  theme: {
    primary: '#1A237E',
    secondary: '#6200EA',
    accent: '#FF5722'
  }
})
Vue.use(BootstrapVue)

Vue.config.devtools = true
Vue.config.performance = true

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
