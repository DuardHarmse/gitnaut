﻿using Gitnaut.Core.Models;
using LibGit2Sharp;
using LibGit2Sharp.Handlers;
using LiteDB;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Gitnaut.Core
{
    public class GitnautCore
    {
        public string Clone(CloneRequest cloneRequest)
        {
            var co = new CloneOptions
            {
                OnProgress = new ProgressHandler((target) =>
                {
                    // Logs progress messages.
                    return true;
                }),
                OnTransferProgress = new TransferProgressHandler((target) =>
                {
                    // Logs download progress.
                    return true;
                }),

                BranchName = cloneRequest.Branch,
                CredentialsProvider = !string.IsNullOrEmpty(cloneRequest.UserName) && !string.IsNullOrEmpty(cloneRequest.Password)
                    ? new CredentialsHandler((_url, _user, _cred) => new UsernamePasswordCredentials
                    {
                        Username = cloneRequest.UserName,
                        Password = cloneRequest.Password
                    })
                    : null
            };
            string repository = Repository.Clone(cloneRequest.Url, cloneRequest.Path, co);

            using (var db = new LiteDatabase("gitnaut.db"))
            {
                var repositories = db.GetCollection<RepositoryDoc>();
                var repoDoc = new RepositoryDoc
                {
                    Name = cloneRequest.Branch,
                    Path = Path.GetFullPath(cloneRequest.Path)
                };

                repositories.Insert(repoDoc);
            }

            return repository;
        }

        public IEnumerable<RepositoryDoc> Repositories()
        {
            using (var db = new LiteDatabase("gitnaut.db"))
            {
                var repositories = db.GetCollection<RepositoryDoc>();
                return repositories.FindAll();
            }
        }

        public Branch Checkout(CheckoutRequest checkoutRequest)
        {
            using (var repo = new Repository(checkoutRequest.Path))
            {
                Branch branch = repo.Branches.FirstOrDefault(b => b.FriendlyName == checkoutRequest.Branch);

                if (branch == null)
                {
                    return branch;
                }

                if (branch.IsRemote)
                {
                    branch = repo.CreateBranch(branch.FriendlyName.Split('/').Last(), branch.FriendlyName);
                }

                Branch currentBranch = Commands.Checkout(repo, branch);

                return currentBranch;
            }
        }
    }
}
