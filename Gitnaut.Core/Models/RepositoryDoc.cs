﻿using System;

namespace Gitnaut.Core.Models
{
    public class RepositoryDoc
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Path { get; set; }
        public string Name { get; set; }
    }
}
