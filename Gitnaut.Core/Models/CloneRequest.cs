﻿namespace Gitnaut.Core.Models
{
    public class CloneRequest
    {
        public string Url { get; set; }
        public string Path { get; set; }
        public string Branch { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
