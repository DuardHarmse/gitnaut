﻿namespace Gitnaut.Core.Models
{
    public class CheckoutRequest
    {
        public string Path { get; set; }
        public string Branch { get; set; }
    }
}
