# Gitnaut
---
Cross-platform git client made using [Chromely](https://github.com/chromelyapps/Chromely), [LibGit2Sharp](https://github.com/libgit2/libgit2sharp), and [Vue.js](https://github.com/vuejs/vue).
